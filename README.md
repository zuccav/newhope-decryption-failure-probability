This C++ script allows to compute the decapsulation failure probability of NewHope following the method described in: Tight bound on NewHope failure probability (https://eprint.iacr.org/2019/1451.pdf) by Thomas Plantard, Arnaud Sipasseuth, Willy Susilo and Vincent Zucca.

It requires to have the gmp (https://gmplib.org/) and mpfr (https://www.mpfr.org/) libraries installed.

If you use this script for your works, please cite the aforementionned paper.