#include <map>
#include <time.h>
#include <string>
#include <math.h>
#include "mpfr.h"
#include <utility>
#include "gmpxx.h"
#include <iostream>

// Compilation line:
// "g++ decryption-failure.cpp -lmpfr -lgmp -lgmpxx -Ofast -o decryption-failure"

// When K1 = K2 one can save the law of <ab / y> by executing the program with: "./decryption-failure --save-law"
// To use a previously computed, and saved, law add -D READ at the compilation line


// Choose the dimension (512 or 1024)
#define N 1024

// Set the binomial parameter to the desired value
#define K1 8 // Binomial parameter of the secret
#define K2 8 // Binomial parameter of the error

// If compiled with "-D MINQ", find the smallest q such that
// decryption error occurs with probability smaller than 2^(-PROBA)
#define PROBA 200

// Define q to get the probability that decryption fails
#ifndef MINQ
#define q 12289
#endif

// Compression parameter
#define t 8

// Precision on x for the binary search in Chernoff bound.
#define prec 0.00001

// Number of bits of precision for floating-point computations
#define PREC 2048

// Discard probability below 2^(-DISCARD) when computing convolutions
#define DISCARD 400

//##################################################################################################
//################################ Do not modify anything below ####################################
//##################################################################################################

#if defined(READ) || defined(MINQ)
#if N == 1024
#if K1 == 1
#include "laws-1024/law-product-1.hpp"
#elif K1 == 2
#include "laws-1024/law-product-2.hpp"
#elif K1 == 3
#include "laws-1024/law-product-3.hpp"
#elif K1 == 4
#include "laws-1024/law-product-4.hpp"
#elif K1 == 5
#include "laws-1024/law-product-5.hpp"
#elif K1 == 6
#include "laws-1024/law-product-6.hpp"
#elif K1 == 7
#include "laws-1024/law-product-7.hpp"
#elif K1 == 8
#include "laws-1024/law-product-8.hpp"
#elif K1 == 9
#include "laws-1024/law-product-9.hpp"
#elif K1 == 10
#include "laws-1024/law-product-10.hpp"
#elif K1 == 11
#include "laws-1024/law-product-11.hpp"
#elif K1 == 12
#include "laws-1024/law-product-12.hpp"
#endif
#elif N == 512
#if K1 == 1
#include "laws-512/law-product-1.hpp"
#elif K1 == 2
#include "laws-512/law-product-2.hpp"
#elif K1 == 3
#include "laws-512/law-product-3.hpp"
#elif K1 == 4
#include "laws-512/law-product-4.hpp"
#elif K1 == 5
#include "laws-512/law-product-5.hpp"
#elif K1 == 6
#include "laws-512/law-product-6.hpp"
#elif K1 == 7
#include "laws-512/law-product-7.hpp"
#elif K1 == 8
#include "laws-512/law-product-8.hpp"
#elif K1 == 9
#include "laws-512/law-product-9.hpp"
#elif K1 == 10
#include "laws-512/law-product-10.hpp"
#elif K1 == 11
#include "laws-512/law-product-11.hpp"
#elif K1 == 12
#include "laws-512/law-product-12.hpp"
#endif
#endif
#endif


/* Return proba than X = l (-k-1 < l < k+1) for X a random variable following a centered binomial law of parameter k. */
mpq_class proba_centered_bino(mpq_t a, unsigned int l, unsigned int k){

  uint64_t num, den;

  mpz_t l_k;
  
  mpz_init(l_k);
  
  mpz_bin_uiui(l_k,2*k,k+l);
  
  num = mpz_get_ui(l_k);
  
  mpz_clear(l_k);

  den = (uint64_t) 1<<(2*k);

  mpq_set_ui(a,num,den);

  return mpq_class(a);
}

void law_centred_bino(std::map<int,mpq_class> &L, int k){

  mpq_t tmp;
  mpq_init(tmp);
  mpq_class pr; 
  
  for(int a = -k ; a <= k ; a++){
    
    pr = proba_centered_bino(tmp,(unsigned int)abs(a),k);
    
    if(L.find(a) != L.end())
      L[a] += pr;
    else
      L.insert(std::make_pair(a,pr));
  }
  
  mpq_clear(tmp);
}

void law_modulus_switching(std::map<int,mpq_class> &L, int Q, int T){

  mpq_class pr(1,Q);

  double tmp;
  
  for(int a = 0 ; a <= Q-1 ; a++){
    tmp = round(a*1.*T/Q);
    tmp = round(tmp*1.*Q/T);
    tmp = a - tmp;
    
    if(L.find(tmp) != L.end())
      L[tmp] += pr;
    else
      L.insert(std::make_pair(tmp,pr));
  }
}

// Compute the law R = L1+L2
void convolution_law(std::map<int,mpq_class> &R, std::map<int,mpq_class> &L1, std::map<int,mpq_class> &L2){

  int a1, a2, tmp;
  mpq_class pr;
  std::map<int,mpq_class>::iterator it1, it2;
  
  for(it1 = L1.begin() ; it1 != L1.end() ; ++it1){
    for(it2 = L2.begin() ; it2 != L2.end() ; ++it2){
      a1 = it1->first;
      a2 = it2->first;
      tmp = a1+a2;
      pr = mpq_class(it1->second);
      pr = pr*(it2->second);

      if(R.find(tmp) != R.end())
	R[tmp] += pr;
      else
	R.insert(std::make_pair(tmp,pr));
    }
  }
  
}

// Compute the law R = [2^d]L and discard element whose proba is below 2^-discard (if discard > 0)
void pow2_law(std::map<int,mpq_class> &R, std::map<int,mpq_class> &L, unsigned int d, unsigned int discard){

  int a1, a2, tmp;
  mpq_class pr, proba_discard, half(1,2);

  if(discard > 0)
    for(unsigned int i = 0 ; i < d ; i++)
      proba_discard *= half;
  
  std::map<int,mpq_class> TMP;

  R = L;
  
  std::map<int,mpq_class>::iterator it1, it2;

  while(d >= 1){
    TMP.clear();
    for(it1 = R.begin() ; it1 != R.end() ; ++it1){
      for(it2 = R.begin() ; it2 != R.end() ; ++it2){
	a1 = it1->first;
	a2 = it2->first;
	tmp = a1+a2;
	pr = mpq_class(it1->second);
	pr = pr*(it2->second);
	
	if(TMP.find(tmp) != TMP.end())
	  TMP[tmp] += pr;
	else
	  TMP.insert(std::make_pair(tmp,pr));
      }
    }
    if(discard > 0)
      for(it1 = TMP.begin() ; it1 != TMP.end() ; ++it1)
	if( cmp(it1->second,proba_discard)<0)
	  TMP.erase(it1);
    
    R = TMP;
    d--;
  }
}

void convert_map(std::map<int,mpq_class> &R, std::map<int,std::string> &L){
  std::map<int,std::string>::iterator it;
  for(it = L.begin() ; it != L.end() ; ++it)
    R.insert(std::make_pair(it->first,mpq_class(it->second)));
}

// Compute the law of a product of elements of S, whose coefficients are given by binomial parameters k1 and k2
void law_product_S(std::map<int,mpq_class> &L, int k1, int k2){
  mpq_t a;
  mpq_init(a);
  mpq_class *pr1 = (mpq_class*) malloc((k1+1)*sizeof(mpq_class));
  mpq_class *pr2 = (mpq_class*) malloc((k2+1)*sizeof(mpq_class));
  for(int i = 0 ; i < k1+1 ; i++)
    pr1[i] = proba_centered_bino(a,(unsigned int)i,k1);
  for(int i = 0 ; i < k2+1 ; i++)
    pr2[i] = proba_centered_bino(a,(unsigned int)i,k2);
  mpq_clear(a);

  int tmp, i=1;
  
#if N == 1024
  
  mpq_class p0, p1, p2, p3, p, r;

  for(int a0 = -k1 ; a0 <= k1 ; a0++){
    for(int a1 = -k1 ; a1 <= k1 ; a1++){
      p0 = pr1[abs(a0)]*pr1[abs(a1)];
      for(int a2 = -k1 ; a2 <= k1 ; a2++){
  	for(int a3 = -k1 ; a3 <= k1 ; a3++){
  	  p1 = pr1[abs(a2)]*pr1[abs(a3)];
  	  r = p0*p1;
  	  for(int b0 = -k2 ; b0 <= k2 ; b0++){
  	    for(int b1 = -k2 ; b1 <= k2 ; b1++){
	      p2 = pr2[abs(b0)]*pr2[abs(b1)];
  	      for(int b2 = -k2 ; b2 <= k2 ; b2++){
  		for(int b3 = -k2 ; b3 <= k2 ; b3++){
		  p3 = pr2[abs(b2)]*pr2[abs(b3)];
  		  p = p2*p3;
  		  p = p*r;

		  tmp = a0*(b0+b1+b2+b3) + a1*(-b3+b0+b1+b2) + a2*(-b2-b3+b0+b1) + a3*(-b1-b2-b3+b0);
		  if(L.find(tmp) != L.end())
  		    L[tmp] += p;
  		  else
  		    L.insert(std::make_pair(tmp,p));
  		}
  	      }
  	    }
  	  }
  	}
      }
    }
    std::cout << (double) 100*i/(k1+k2+1) << " \%" << std::endl;
    i++;
  }
  
#elif N == 512
  mpq_class p0, p1, p;

  for(int a0 = -k1 ; a0 <= k1 ; a0++){
    for(int a1 = -k1 ; a1 <= k1 ; a1++){
      p0 = pr1[abs(a0)]*pr1[abs(a1)];
      for(int b0 = -k2 ; b0 <= k2 ; b0++){
	for(int b1 = -k2 ; b1 <= k2 ; b1++){
	  p1 = pr2[abs(b0)]*pr2[abs(b1)];
	  p = p0*p1;

	  tmp = a0*(b0+b1) + a1*(b0-b1);
	  if(L.find(tmp) != L.end())
	    L[tmp] += p;
	  else
	    L.insert(std::make_pair(tmp,p));
	}
      }
    }
    std::cout << (double) 100*i/(k1+k2+1) << " \%" << std::endl;
    i++;
  }
#endif

  free(pr1);
  free(pr2);
}

void write(const char *file_name, std::map<int,mpq_class> M){

  FILE* f = fopen(file_name,"w+");

  if(f == NULL){
    std::cout << "Error: cannot open the file!" << std::endl;
    exit(1);
  }
  
  std::map<int,mpq_class>::iterator it;
  fprintf(f,"#ifndef _LAW_PRODUCT_\n\nstd::map<int,std::string> L = {");
  for(it = M.begin() ; it != --M.end() ; ++it)
    fprintf(f,"{%d,\"%s\"}, ",it->first,it->second.get_str().c_str());
  
  fprintf(f,"{%d,\"%s\"} };\n\n#endif",it->first,it->second.get_str().c_str());
  
  fclose(f);
}


// Compute the moment of M in x and store it in m
void moment(mpfr_t &m, const mpq_class &x, std::map<int,mpq_class> &M){
  mpfr_set_ui(m,0,MPFR_RNDN);

  std::map<int,mpq_class>::iterator it;

  mpq_class tmp;
  mpfr_t exp;
  mpfr_init2(exp,PREC);
  for(it = M.begin() ; it != M.end() ; ++it){
    tmp = mpq_class(it->first);
    tmp *= x;
    mpfr_set_q(exp,tmp.get_mpq_t(),MPFR_RNDN);
    mpfr_exp(exp,exp,MPFR_RNDN);
    mpfr_mul_q(exp,exp,it->second.get_mpq_t(),MPFR_RNDN);
    mpfr_add(m,m,exp,MPFR_RNDN);
  }
  
  mpfr_clear(exp);  
}

// Compute an upper-bound (depending of x) on P(X >= a) for a random variable X whose law is given by M.
void Chernoff_bound(mpfr_t &res, const mpq_class &x, const mpq_class &a, std::map<int,mpq_class> &M, std::map<int,mpq_class> &L, unsigned int Q){

  mpfr_t restmp;
  mpfr_init2(restmp,PREC);
  
  mpq_class tmp = -x*a;
  
  moment(res,x,M); // law of a produt of S
  moment(restmp,x,L);
  
  mpfr_log(res,res,MPFR_RNDN);
  mpfr_log(restmp,restmp,MPFR_RNDN);
 
  mpfr_mul_ui(res,res,512,MPFR_RNDN);
  mpfr_mul_ui(restmp,restmp,N/256,MPFR_RNDN);
   
  mpfr_add(res,res,restmp,MPFR_RNDN);
  mpfr_add_q(res,res,tmp.get_mpq_t(),MPFR_RNDN);
  
  mpfr_exp(res,res,MPFR_RNDN);

  mpfr_clear(restmp);
}

// Bound on the noise for correct decryption
mpq_class bound_noise(unsigned int Q){
  
  mpq_class B;

#if N == 1024
  if(Q%2 == 1)
    B = mpq_class(Q-1);
  else
    B = mpq_class(Q);
#elif N == 512
  B = mpq_class(Q>>1);
#endif
  return B;
}

void proba_bound_decryption_failure(mpfr_t &res, mpq_class &B, std::map<int,mpq_class> &M, unsigned int Q){
  // Binary Search of the minimum proba;
  mpq_class ta(prec), tb(30), tc;  
  
  mpfr_t a, b, c;
  mpfr_inits2(PREC,a,b,c,NULL);

  std::map<int, mpq_class> L, L1, L2;

  law_modulus_switching(L1,Q,t);
  law_centred_bino(L2,K2);

  // Compute the convolution of the two laws
  convolution_law(L,L1,L2);
  
  Chernoff_bound(a,ta,B,M,L,Q);
  Chernoff_bound(b,tb,B,M,L,Q);
  
  while(cmp(tb-ta, prec) > 0){
    
    tc = (ta+tb)/2;
    Chernoff_bound(c,tc,B,M,L,Q);
    
    if(mpfr_cmp(a,b) < 0){
      if(mpfr_cmp(c,b) < 0){
	tb = tc;
	mpfr_set(b,c,MPFR_RNDN);	  
      }
      else{
	mpfr_set(c,a,MPFR_RNDN);
	tb-=ta;
	std::cout << "Warning larger precision than expected (" << tb.get_d() << ")\n";
      }
    }
    else{
      if(mpfr_cmp(c,a) < 0){
	ta = tc;
	mpfr_set(a,c,MPFR_RNDN);	  
      }
      else{
	mpfr_set(c,b,MPFR_RNDN);
	tb-=ta;
	std::cout << "Warning larger precision than expected (" << tb.get_d() << ")\n";
	goto done;
      }
    }
  }
  std::cout << "Bound found for t = " << tc.get_d() << std::endl;
 done:

#if N == 512
  mpfr_mul_ui(c,c,1024,MPFR_RNDN); // 2^2 choices of y and 2^8 choices i.
#elif N == 1024
  mpfr_mul_ui(c,c,4096,MPFR_RNDN); // 2^4 choices of y and 2^8 choices of i
#endif
  
  mpfr_log2(res,c,MPFR_RNDN);

  mpfr_clears(a,b,c,NULL);
}

void proba_bound_decryption_failure(mpfr_t &res, mpq_class &B, std::map<int,std::string> &M, unsigned int Q){
  std::map<int,mpq_class> M1;
  convert_map(M1,M);
  proba_bound_decryption_failure(res,B,M1,Q);
}


void proba_bound_decryption_failure_simulated(mpfr_t &res, mpq_class &B, std::map<int,mpq_class> &M, unsigned int Q){
  // Compute the law and read the proba
  mpq_class P(0);
  
  std::map<int,mpq_class> MTMP1(M), MTMP2, MTMP3, MTMP4, MTMP5;

  // Compute [2^9]MTMP1
  pow2_law(MTMP1,MTMP1,9,DISCARD);
    
  law_centred_bino(MTMP2,K2);
  law_modulus_switching(MTMP3,Q,t);
  
  convolution_law(MTMP4,MTMP2,MTMP3);

  if( N == 1024)
    pow2_law(MTMP4,MTMP4,2,DISCARD);
  if( N == 512)
    pow2_law(MTMP4,MTMP4,1,DISCARD);
  
  convolution_law(MTMP5,MTMP1,MTMP4);

  std::map<int,mpq_class>::iterator it;
  
  for(it = MTMP5.begin() ; it != --MTMP5.end() ; ++it){
    if( cmp(it->first,B)>=0)
      P+=it->second;
  }
  
#if N == 512
   P*=1024; // 2^2 choices of y and 2^8 choices i.
#elif N == 1024
   P*=4096; // 2^4 choices of y and 2^8 choices of i
#endif

   mpfr_set_ui(res,(unsigned long)1,MPFR_RNDN);
   
   mpfr_mul_q(res,res,P.get_mpq_t(),MPFR_RNDN);
   
   mpfr_log2(res,res,MPFR_RNDN);
}

void proba_bound_decryption_failure_simulated(mpfr_t &res, mpq_class &B, std::map<int,std::string> &M, unsigned int Q){
  std::map<int,mpq_class> M1;
  convert_map(M1,M);
  proba_bound_decryption_failure_simulated(res,B,M1,Q);
}

// Find the minimal q one can use to ensure a probability of decryption failure <= 2^(-logproba)
unsigned int minimalq(unsigned int logproba, std::map<int,std::string> L){

  unsigned int qmin = 512, qmax = 12289, res;

  mpfr_t proba, p;

  mpq_class B;
  
  mpfr_inits2(PREC,proba,p,NULL);

  mpfr_set_si(proba,-PROBA,MPFR_RNDN);

  mpfr_set_ui(p,0,MPFR_RNDN);
  
  while(qmax-qmin > 1){
    
    res = (qmin+qmax)>>1;
    B = bound_noise(res);
    proba_bound_decryption_failure(p,B,L,res);

    std::cout << "q = " << res << " proba = ";
    mpfr_out_str(stdout,10,5,p,MPFR_RNDN);
    std::cout << "\n" << std::endl;
				  
    if (mpfr_cmp(p,proba) <= 0)
      qmax = res;
    else
      qmin = res;
  }

  if(p > proba)
    res +=1;
    
  mpfr_clears(proba,p,NULL);
  
  return res;
}


int main(int argc, char* argv[]){

  std::cout << "\n########################################## NewHope" << N << " ##########################################" << std::endl;

  clock_t t1, t2;
  
#ifndef MINQ
  std::cout << "############################ For k1 = " << K1 << ", k2 = " << K2 << ", t = " << t << " and q = " << q << " ############################\n" << std::endl;

  // Compute the probability from the pre-computed law
#ifdef READ

  //Bound on the noise:
  mpq_class B = bound_noise(q);
  
  std::cout << "B = " << B.get_d() << std::endl;

  std::cout << "Size of the map: " << L.size() << "\n" << std::endl;
  
  mpfr_t res;
  mpfr_init2(res,PREC);
  std::cout << "Probability computed using Chernoff bound:"<< std::endl;
  t1 = clock();
  proba_bound_decryption_failure(res,B,L,q);
  t2 = clock();
  std::cout << "Pr(decryption failure) <= 2^(";
  mpfr_out_str(stdout,10,5,res,MPFR_RNDN);
  std::cout << ") computed in " << (double)(t2-t1)/CLOCKS_PER_SEC << " s with a precision of " << PREC << " bits.\n" << std::endl;

  if(K2 < 3){
    std::cout << "Probability computed using only simulations:"<< std::endl;
    t1 = clock();
    proba_bound_decryption_failure_simulated(res, B, L, q);
    t2 = clock();
    std::cout << "Pr(decryption failure) <= 2^(";
    mpfr_out_str(stdout,10,5,res,MPFR_RNDN);
    std::cout << ") computed in " << (double)(t2-t1)/CLOCKS_PER_SEC << " s with the discard limit set to probability 2^(-" << DISCARD << ").\n" << std::endl;
  }

#else
  
  std::cout << "Start the computation of the law of a product in S..." << std::endl;
  std::map<int,mpq_class> M;
  
  t1 = clock();
  law_product_S(M,K1,K2);
  t2 = clock();
  std::cout << "Law of the product computed in: " << (double)(t2-t1)/CLOCKS_PER_SEC << " s" << std::endl;
  
  std::cout << "Size of the map: " << M.size() << "\n" << std::endl;

  // Eventually write the law of Z
  if(argc == 2 && (std::string) argv[1] == "--save-law"){
    const char *file_name;
#if N == 1024
    if( K1 == 1 && K2 == 1)
      file_name = "laws-1024/law-product-1.hpp";
    else  if( K1 == 2)
      file_name = "laws-1024/law-product-2.hpp";
    else if( K1 == 3)
      file_name = "laws-1024/law-product-3.hpp";
    else if( K1 == 4)
      file_name = "laws-1024/law-product-4.hpp";
    else if( K1 == 5)
      file_name = "laws-1024/law-product-5.hpp";
    else if( K1 == 6)
      file_name = "laws-1024/law-product-6.hpp";
    else if( K1 == 7)
      file_name = "laws-1024/law-product-7.hpp";
    else if( K1 == 8)
      file_name = "laws-1024/law-product-8.hpp";
    else if( K1 == 9)
      file_name = "laws-1024/law-product-9.hpp";
    else if( K1 == 10)
      file_name = "laws-1024/law-product-10.hpp";
    else if( K1 == 11)
      file_name = "laws-1024/law-product-11.hpp";
    else if( K1 == 12)
      file_name = "laws-1024/law-product-12.hpp";
    else{
      std::cout << "k must be <= 12!" << std::endl;
      exit(1);
    }
#elif N == 512
    if( K1 == 1 && K2 == 1)
      file_name = "laws-512/law-product-1.hpp";
    else  if( K1 == 2)
      file_name = "laws-512/law-product-2.hpp";
    else if( K1 == 3)
      file_name = "laws-512/law-product-3.hpp";
    else if( K1 == 4)
      file_name = "laws-512/law-product-4.hpp";
    else if( K1 == 5)
      file_name = "laws-512/law-product-5.hpp";
    else if( K1 == 6)
      file_name = "laws-512/law-product-6.hpp";
    else if( K1 == 7)
      file_name = "laws-512/law-product-7.hpp";
    else if( K1 == 8)
      file_name = "laws-512/law-product-8.hpp";
    else if( K1 == 9)
      file_name = "laws-512/law-product-9.hpp";
    else if( K1 == 10)
      file_name = "laws-512/law-product-10.hpp";
    else if( K1 == 11)
      file_name = "laws-512/law-product-11.hpp";
    else if( K1 == 12)
      file_name = "laws-512/law-product-12.hpp";
    else{
      std::cout << "k must be <= 12!" << std::endl;
      exit(1);
    }
#endif
    write(file_name,M);
  }

  //Bound on the noise:
  mpq_class B = bound_noise(q);

  std::cout << "B = " << B.get_d() << std::endl;
  
  mpfr_t res;
  mpfr_init2(res,PREC);
  std::cout << "Probability computed using Chernoff bound:"<< std::endl;
  t1 = clock();
  proba_bound_decryption_failure(res,B,M,q);
  t2 = clock();
  std::cout << "Pr(decryption failure) <= 2^(";
  mpfr_out_str(stdout,10,5,res,MPFR_RNDN);
  std::cout << ") computed in " << (double)(t2-t1)/CLOCKS_PER_SEC << " s with a precision of " << PREC << " bits.\n" << std::endl;

  if(K2 < 3){
    std::cout << "Probability computed using only simulations:"<< std::endl;
    t1 = clock();
    proba_bound_decryption_failure_simulated(res, B, M, q);
    t2 = clock();
    std::cout << "Pr(decryption failure) <= 2^(";
    mpfr_out_str(stdout,10,5,res,MPFR_RNDN);
    std::cout << ") computed in " << (double)(t2-t1)/CLOCKS_PER_SEC << " s with the discard limit set to probability 2^(-" << DISCARD << ").\n" << std::endl;
  }
  
  
#endif

  // Compute the minimal q
#elif MINQ

  t1 = clock();
  unsigned int minq = minimalq(PROBA,L);
  t2 = clock();

  std::cout << "qmin = " << minq << " (for k1 = " << K1 << ", k2 = " << K2 << ", t = " << t << " and proba <= 2^(" << -PROBA << ") ) computed in " << (double)(t2-t1)/CLOCKS_PER_SEC << " s\n" << std::endl;

#endif
  
  return 0;
}
